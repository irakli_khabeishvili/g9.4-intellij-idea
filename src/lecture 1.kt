import kotlin.math.sqrt
import kotlin.math.pow

class Point(var a:Int, var b:Int){
    override fun toString():String{
        return("$a,$b")
    }
    override fun equals(other:Any?):
            Boolean{
        if(other is Point){
            if(a==other.a && b==other.b){
                return true
            }
        }
        return false
    }
    fun toSymmetry():Point{
        val clone=Point(a,b)
        clone.a=-a
        clone.b=-b
        return clone
    }
    fun calcDistance(anotherPoint:Point):Double{
        return sqrt((anotherPoint.a-a).toDouble().pow(2)+(anotherPoint.b-b).toDouble().pow(2))
    }

}
//2
class Fraction(var numerator:Double=0.0,var denominator:Double=0.0){
    override fun equals(other:Any?):
Boolean{
        if(other is Fraction){
            if(numerator / denominator==other.numerator/other.denominator){
                return true
            }
        }
        return false
    }
    fun reduceFraction(num:Double,returnNum:Boolean):Any{
        if(returnNum){
            return(numerator/num)/(denominator/num)
        }
        val clone=Fraction(numerator,denominator)
        clone.numerator/=num
        clone.denominator/=num
        return clone
    }
    fun subtractFraction(anotherFraction:Fraction):Double{
        return(numerator/denominator)-(anotherFraction.numerator/anotherFraction.denominator)
    }
    fun addFraction(anothrFraction:Fraction):Double{
        return(numerator/denominator)+(anotherFraction.numerator/anotherFraction.denuminator)
    }
    fun divideFraction(anotherFraction:Fraction):Double{
        return(numerator/denominator)/(anotherFraction.numerator/anotherFraction.denominator)
    }
    fun multiplyFraction(anotherFraction:Fraction):Double{
        return(numerator/denominator)*(anotherFraction.numerator/anotherFraction.denominator)
    }
}
